<?php
/* Copyright (C) 2019  Stephan Kreutzer
 *
 * This file is part of SOS Server Prototype 1.
 *
 * SOS Server Prototype 1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * SOS Server Prototype 1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with SOS Server Prototype 1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/libraries/user_management.inc.php
 * @author Stephan Kreutzer
 * @since 2019-10-13
 */



require_once(dirname(__FILE__)."/database.inc.php");
require_once(dirname(__FILE__)."/poi_defines.inc.php");



function GetPois($type = null)
{
    if ($type !== null)
    {
        if (is_numeric($type) != true)
        {
            return -1;
        }
    }

    if (Database::Get()->IsConnected() !== true)
    {
        return -1;
    }

    $pois = Database::Get()->QueryUnsecure("SELECT `".Database::Get()->GetPrefix()."pois`.`id` AS `pois_id`,\n".
                                           "    `".Database::Get()->GetPrefix()."pois`.`type` AS `pois_type`,\n".
                                           "    `".Database::Get()->GetPrefix()."pois`.`id_pois` AS `pois_id_pois`,\n".
                                           "    `".Database::Get()->GetPrefix()."poi_revisions`.`id` AS `poi_revisions_id`,\n".
                                           "    `".Database::Get()->GetPrefix()."poi_revisions`.`text` AS `poi_revisions_text`,\n".
                                           "    `".Database::Get()->GetPrefix()."poi_revisions`.`revision_datetime` AS `poi_revisions_revision_datetime`\n".
                                           "FROM `".Database::Get()->GetPrefix()."poi_revisions`\n".
                                           "INNER JOIN `".Database::Get()->GetPrefix()."pois` ON\n".
                                           "    `".Database::Get()->GetPrefix()."pois`.`id` =\n".
                                           "    `".Database::Get()->GetPrefix()."poi_revisions`.`id_pois`\n".
                                           "WHERE 1\n". // `".Database::Get()->GetPrefix()."pois`.`id_pois` IS NULL\n".
                                           "ORDER BY `".Database::Get()->GetPrefix()."pois`.`id` DESC,\n".
                                           "    `".Database::Get()->GetPrefix()."poi_revisions`.`revision_datetime` DESC");

    if (is_array($pois) !== true)
    {
        return -1;
    }

    return $pois;
}



?>
