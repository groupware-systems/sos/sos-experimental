/* Copyright (C) 2019 Stephan Kreutzer
 *
 * This file is part of SOS Server Prototype 1.
 *
 * SOS Server Prototype 1 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * SOS Server Prototype 1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with SOS Server Prototype 1. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

let gridNavigator = new SosGridNavigator();

function SosGridNavigator()
{
    let that = this;

    that.NavigateIssue = function(issueId) {
        Reset();
        ResetDetails();

        _currentIssue = _gridEngine.getIssueById(issueId);

        if (_currentIssue == null)
        {
            console.log("Loading an Issue failed.");
            return false;
        }

        if (RenderIssue(_currentIssue) != true)
        {
            return false;
        }

        return true;
    }

    that.NavigateList = function() {
        Reset();
        ResetDetails();

        if (_issueListLoaded !== true)
        {
            RenderIssueList(_gridEngine.getIssueList());
            _issueListLoaded = true;
        }

        RenderShowIssueList();
    }

    that.ShowDetails = function(id, type) {
        ResetDetails();

        if (_currentIssue == null)
        {
            console.log("SosGridNavigator.ShowDetails() without _currentIssue.");
            return -1;
        }

        let poi = null;

        if (type === "current")
        {
            poi = _currentIssue;
        }
        else if (type == "cause")
        {
            for (let i = 0; i < _currentIssue.getCauses().length; i++)
            {
                if (_currentIssue.getCauses()[i].getId() === id)
                {
                    poi = _currentIssue.getCauses()[i];
                    break;
                }
            }
        }
        else if (type == "effect")
        {
            for (let i = 0; i < _currentIssue.getEffects().length; i++)
            {
                if (_currentIssue.getEffects()[i].getId() === id)
                {
                    poi = _currentIssue.getEffects()[i];
                    break;
                }
            }
        }
        else
        {
            console.log("Type '" + type + "' not supported.");
            return -1;
        }

        if (poi == null)
        {
            console.log("Couldn't find POI with ID '" + id + "'.");
            return -1;
        }

        if (RenderDetails(poi) != 0)
        {
            return -1;
        }

        return 0;
    }

    let _gridEngine = new SosGridEngine();
    let _currentIssue = null;
    let _issueListLoaded = false;
}
