<?php
/* Copyright (C) 2017-2019  Stephan Kreutzer
 *
 * This file is part of SOS Server Prototype 1.
 *
 * SOS Server Prototype 1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * SOS Server Prototype 1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with SOS Server Prototype 1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/en/admin_import.lang.php
 * @author Stephan Kreutzer
 * @since 2019-10-19
 */



define("LANG_PAGETITLE", "Import");
define("LANG_HEADER", "Import");
define("LANG_IMPORT_DESCRIPTION", "Import a SOS file. <span style=\"color:red;\">WARNING: This will IRRECOVERABLY DELETE all current SOS data in the database!</span>");
define("LANG_IMPORT_SUBMIT", "Import");
define("LANG_LINKCAPTION_CANCEL", "Cancel");
define("LANG_UPLOAD_GENERAL_ERROR", "A general error occurred.");
define("LANG_UPLOAD_SPECIFIC_ERROR_PRE", "Error ");
define("LANG_UPLOAD_SPECIFIC_ERROR_POST", " has occurred.");
define("LANG_LINKCAPTION_DONE", "Done");
define("LANG_LINKCAPTION_BACK", "Back");
define("LANG_IMPORT_SUCCESS", "File import was successful.");
define("LANG_IMPORT_FAILURE", "The import failed.");



?>
