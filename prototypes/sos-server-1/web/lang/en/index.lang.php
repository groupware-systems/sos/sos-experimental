<?php
/* Copyright (C) 2014-2019  Stephan Kreutzer
 *
 * This file is part of SOS Server Prototype 1.
 *
 * SOS Server Prototype 1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * SOS Server Prototype 1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with SOS Server Prototype 1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/en/index.lang.php
 * @author Stephan Kreutzer
 * @since 2014-05-31
 */



define("LANG_PAGETITLE", "Welcome!");
define("LANG_HEADER", "Welcome!");
define("LANG_INSTALLBUTTON", "Install");
define("LANG_INSTALLDELETEFAILED", "The installation was already completed successfully, but it was unable to delete itself. Please delete at least the file <tt>\$/install/install.php</tt> or additionally the entire directory <tt>\$/install/</tt> manually.");
define("LANG_WELCOMETEXT", "Welcome to the server prototype 1 for <a href=\"http://untiednations.com/community/plan-sos\">Shared/Semantic/Structured Open Space</a>!");
define("LANG_LOGINDESCRIPTION", "Login for participants:");
define("LANG_NAMEFIELD_CAPTION", "Name");
define("LANG_PASSWORDFIELD_CAPTION", "Password");
define("LANG_SUBMITBUTTON", "OK");
define("LANG_DBCONNECTFAILED", "Can’t connect to database.");
define("LANG_LOGINSUCCESS", "Login was successful!");
define("LANG_LOGINFAILED", "Password incorrect!");
define("LANG_LASTLOGIN_PRE", "The last login occurred on ");
define("LANG_LASTLOGIN_POST", ". If you didn’t login on this date and time, please contact the operator/administrator.");
define("LANG_LINKCAPTION_GRID", "Grid");
define("LANG_LINKCAPTION_CONTINUE", "Continue");
define("LANG_BUTTON_LOGOUT", "Log out");
define("LANG_LINKCAPTION_RETRYLOGIN", "Retry");
define("LANG_LINKCAPTION_IMPORT", "Import");
//define("LANG_LINKCAPTION_ADMINUSERCREATE", "Create new user");
define("LANG_LICENSE", "Licensing");



?>
