<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright 2018-2021 Christopher Gutteridge, Stephan Kreutzer

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:sos="http://www.untiednations.com/SOS">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

  <xsl:template match="/">
    <html version="-//W3C//DTD XHTML 1.1//EN" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd" xml:lang="en" lang="en">
      <head>
        <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
<xsl:text>&#xA;</xsl:text>
<xsl:comment> This file was created by sos_xml_to_sos_map_xhtml_1.xsl of SOS Map Prototype 1, which is free software licensed under the Apache License 2.0 (see https://gitlab.com/groupware-systems/sos/sos-experimental/tree/master/prototypes/sos-map-1/ and http://www.untiednations.com/community/plan-sos/). </xsl:comment>
<xsl:text>&#xA;</xsl:text>
<xsl:comment>
Copyright 2018-2021 Christopher Gutteridge, Stephan Kreutzer

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

The data in the LQS.setLayout() argument is not part of this program,
it's user data that is only processed. A different license may apply.
</xsl:comment>
<xsl:text>&#xA;</xsl:text>
        <title>SOS Map Prototype 1</title>
        <meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport"/>

        <!-- <script src="...">//</script> to prevent self-closing by XSLT outputting XHTML. -->
        <script type="text/javascript" src="js/jquery-1.12.4.min.js">//</script>
        <script type="text/javascript" src="js/jquery-ui.js">//</script>
        <script type="text/javascript" src="js/jquery.ui.touch-punch.min.js">//</script>
        <script type="text/javascript" src="js/hammer.min.js">//</script>
        <script type="text/javascript" src="js/lqs/core.js">//</script>
        <script type="text/javascript" src="js/lqs/point.js">//</script>
        <script type="text/javascript" src="js/lqs/line.js">//</script>
        <script type="text/javascript" src="js/lqs/node.js">//</script>
        <script type="text/javascript" src="js/lqs/link.js">//</script>
        <script type="text/javascript" src="js/lqs/viewspec.js">//</script>
        <script type="text/javascript" src="js/lqs/node-text.js">//</script>
        <script type="text/javascript" src="js/lqs/node-html.js">//</script>
        <script type="text/javascript" src="js/lqs/node-error.js">//</script>
        <script type="text/javascript" src="js/lqs/node-embed.js">//</script>
        <script type="text/javascript" src="js/lqs/node-cited.js">//</script>

        <link rel="stylesheet" type="text/css" href="css/jquery-ui.css"/>
        <link rel="stylesheet" type="text/css" href="css/sosmap.css"/>

        <script type="text/javascript">
<xsl:text>
document.addEventListener('DOMContentLoaded', function() {
	var lqs = new LQS();
	// The data passed as argument to this call is not part of this program, it's user data and might be under a different license than this program. This program also doesn't depend on it or link it as a library, it's merely processed.
	lqs.setLayout( {
		nodes: [
</xsl:text>
<xsl:apply-templates select="/sos:SOS/sos:POI"/>
<xsl:text>
		],
		links: [
</xsl:text>
<xsl:apply-templates select="/sos:SOS/sos:POI/sos:Parent"/>
<xsl:text>
		]
	});
	lqs.updateAllPositions();
	lqs.centrePage();
});
</xsl:text>
        </script>
      </head>
      <body>

      </body>
    </html>
  </xsl:template>

  <xsl:template match="/sos:SOS/sos:POI">
    <!-- TODO: Things break if JSON special characters are in the input. It's a bad idea to mix different format conventions, and as XSLT has trouble of dealing with them, we might need to build a dedicated standalone generator tool. -->
    <xsl:text>{id: '</xsl:text>
    <xsl:value-of select="@ID"/>
    <xsl:text>', pos: { x: </xsl:text>
    <xsl:value-of select="position()-1"/>
    <!--xsl:text> * 400, y: 0 }, size: { width: 200, height: 200 }, title: '</xsl:text-->
    <xsl:text> * 0, y: 0 }, size: { width: 200, height: 200 }, title: '</xsl:text>
    <xsl:value-of select="./sos:Title//text()"/>
    <xsl:text>', type: 'text', text: '</xsl:text>
    <xsl:value-of select="./sos:Text//text()"/>
    <xsl:text>'},</xsl:text>
  </xsl:template>

  <xsl:template match="/sos:SOS/sos:POI/sos:Parent">
    <xsl:text>{label:"</xsl:text>
    <xsl:choose>
      <xsl:when test="../@Type='Issue'">
        <!-- This is for arrows pointing to the Issue, ignoring the type of the source POI. -->
        <xsl:text>causes</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>???</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>",id:"connection-</xsl:text>
    <xsl:value-of select="./@Parent-ID"/>
    <xsl:text>-</xsl:text>
    <xsl:value-of select="../@ID"/>
    <xsl:text>",subject:{node:"</xsl:text>
    <xsl:value-of select="./@Parent-ID"/>
    <xsl:text>"},object:{node:"</xsl:text>
    <xsl:value-of select="../@ID"/>
    <xsl:text>"}},</xsl:text>
  </xsl:template>

</xsl:stylesheet>
