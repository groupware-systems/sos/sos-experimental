<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2018-2019 Stephan Kreutzer

This file is part of SOS Grid Prototype 3.

SOS Grid Prototype 3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

SOS Grid Prototype 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with SOS Grid Prototype 3. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:sos="http://www.untiednations.com/SOS">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

  <xsl:template match="/">
    <html version="-//W3C//DTD XHTML 1.1//EN" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd" xml:lang="en" lang="en">
      <head>
        <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
<xsl:text>&#xA;</xsl:text>
<xsl:comment> This file was created by sos_xml_to_sos_grid_xhtml_1.xsl of SOS Grid Prototype 3, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/groupware-systems/sos/sos-experimental/tree/master/prototypes/sos-grid-3/ and http://www.untiednations.com/community/plan-sos/). </xsl:comment>
<xsl:text>&#xA;</xsl:text>
<xsl:comment>
Copyright (C) 2018-2019 Stephan Kreutzer

This file is part of SOS Grid Prototype 3.

SOS Grid Prototype 3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

SOS Grid Prototype 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with SOS Grid Prototype 3. If not, see &lt;http://www.gnu.org/licenses/&gt;.

The data in the &lt;div id="sos-input"/&gt; is not part of this program,
it's user data that is only processed. A different license may apply.
</xsl:comment>
<xsl:text>&#xA;</xsl:text>
        <title>SOS Grid Prototype 3</title>
        <meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport"/>
        <link rel="stylesheet" type="text/css" href="./css/styles.css"/>

        <!-- <script src="...">//</script> to prevent self-closing by XSLT outputting XHTML. -->
        <script type="text/javascript" src="./js/sos-grid-engine.js">//</script>
        <script type="text/javascript" src="./js/sos-grid-renderer.js">//</script>
        <script type="text/javascript" src="./js/sos-grid-navigation.js">//</script>

        <script type="text/javascript">
<xsl:text>
"use strict";

// As browsers/W3C/WHATWG are incredibly evil, they might ignore the self-declarative
// XML namespace of this document and the given content type in the header, and instead
// assume/render "text/html", which then fails with JavaScript characters that are
// XML/XHTML special characters which need to be escaped, if the file is saved under
// a name that happens to end with ".html" (!!!). Just have the file name end with
// ".xhtml" and it might magically start to work.

function loadGrid()
{
    if (window.location.hash.length > 1)
    {
        if (gridNavigator.NavigateIssue(window.location.hash.substr(1)) !== true)
        {
            gridNavigator.NavigateList();
        }
    }
    else
    {
        gridNavigator.NavigateList();
    }
}

window.onload = function () {
    let loadLink = document.getElementById('loadLink');
    loadLink.parentNode.removeChild(loadLink);

    loadGrid();

    if ("onhashchange" in window)
    {
        window.onhashchange = loadGrid;
    }
};
</xsl:text>
        </script>
      </head>
      <body>
        <div id="grid">
          <div id="causes" class="causes"/>
          <div id="current" class="current hidden"/>
          <div id="list" class="current hidden"/>
          <div id="effects" class="effects"/>
          <div id="details" class="details"/>
          <div id="loadLink">
            <a href="#" onclick="loadGrid();">Load</a>
          </div>
        </div>

        <!--
            As the stupid browser execution environment is sandboxed, local files can't
            be dynamically read from disk, and as we're forced to load the entire source
            into memory anyway because of this, if it's XML, we could easily put it into
            the DOM instead of parsing XML from a JavaScript variable. This interferes
            with XHTML GUI work in this XSLT file, but if the XML source would need to be
            escaped to be a JavaScript string literal, a conversion is unavoidable, so
            the XHTML file can just as well be the result of a XSLT conversion from the source.
        -->
        <div id="sos-input" style="display:none;">
          <xsl:comment> The data contained in this element and sub-elements is not part of this program, it's user data and might be under a different license than this program. This program also doesn't depend on it or link it as a library, it's only processed. </xsl:comment>
          <xsl:apply-templates select="/sos:SOS"/>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="//sos:*">
    <xsl:element name="sos:{local-name()}">
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates select="node()|text()"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="//sos:*//text()">
    <xsl:copy/>
  </xsl:template>

  <xsl:template match="text()"/>

</xsl:stylesheet>
