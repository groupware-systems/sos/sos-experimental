/* Copyright (C) 2019 Stephan Kreutzer
 *
 * This file is part of SOS Grid Prototype 3.
 *
 * SOS Grid Prototype 3 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * SOS Grid Prototype 3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with SOS Grid Prototype 3. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function SosGridEngine()
{
    let that = this;

    // Pass the ID of an Issue, Cause or Effect, which is a string. Empty string
    // to get the first, default Issue. Returns null if Issue can't be obtained.
    that.getIssueById = function(id) {
        if (typeof id !== 'string' && !(id instanceof String))
        {
            throw "ID of invalid type.";
        }

        if (_loaded == false)
        {
            _load();
        }

        if (_loaded == false)
        {
            throw "Loading failed."
        }

        let issueSource = null;

        if (!(id in _issueMapping))
        {
            return null;
        }

        issueSource = _issueMapping[id];

        if (issueSource == null)
        {
            return null;
        }

        if (issueSource.hasAttribute("id") != true)
        {
            throw "Issue is missing its 'id' attribute.";
        }

        // TODO: The code to read in the entire Issue with its Causes and Effects could
        // be written much more smarter.

        let title = null;
        let description = null;
        let causes = new Array();
        let effects = new Array();

        for (let i = 0; i < issueSource.children.length; i++)
        {
            let child = issueSource.children[i];

            if (child.namespaceURI != "http://www.untiednations.com/SOS")
            {
                continue;
            }

            if (child.localName == "Title")
            {
                if (title != null)
                {
                    throw "Issue contains more than one Title.";
                }

                title = child.textContent;
            }
            else if (child.localName == "Text")
            {
                if (description != null)
                {
                    throw "Issue contains more than one Text.";
                }

                description = child.textContent;
            }
            else if (child.localName == "Cause" ||
                     child.localName == "Effect")
            {
                if (child.hasAttribute("id") != true)
                {
                    throw "Child is missing its 'id' attribute.";
                }

                let childId = child.getAttribute("id");

                if (childId.length <= 0)
                {
                    throw "'id' attribute value of Child is empty.";
                }

                if (child.localName == "Cause")
                {
                    if (causes.includes(childId) == true)
                    {
                        throw "'id' attribute value of Cause '" + childId + "' more than once.";
                    }
                }
                else if (child.localName == "Effect")
                {
                    if (effects.includes(childId) == true)
                    {
                        throw "'id' attribute value of Effect '" + childId + "' more than once.";
                    }
                }

                if (childId === issueSource.getAttribute("id"))
                {
                    throw "'id' attribute value of Child '" + childId + "' more than once.";
                }

                let childTitle = null;
                let childDescription = null;

                for (let j = 0; j < child.children.length; j++)
                {
                    if (child.children[j].namespaceURI != "http://www.untiednations.com/SOS")
                    {
                        continue;
                    }

                    if (child.children[j].localName === "Title")
                    {
                        if (childTitle != null)
                        {
                            throw "Child contains more than one Title.";
                        }

                        childTitle = child.children[j].textContent;
                    }
                    else if (child.children[j].localName === "Text")
                    {
                        if (childDescription != null)
                        {
                            throw "Child contains more than one Text.";
                        }

                        childDescription = child.children[j].textContent;
                    }
                }

                if (child.localName == "Cause")
                {
                    causes.push(new Cause(childId, childTitle, childDescription));
                }
                else if (child.localName == "Effect")
                {
                    effects.push(new Effect(childId, childTitle, childDescription));
                }
            }
        }

        return new Issue(issueSource.getAttribute("id"), title, description, causes, effects);
    }

    that.getIssueList = function() {
        if (_loaded == false)
        {
            _load();
        }

        if (_loaded == false)
        {
            throw "Loading failed."
        }

        return _issueList;
    }

    function _load()
    {
        if (_loaded != false)
        {
            return true;
        }

        // Stupid JavaScript/web/browsers: document.getElementsByTagNameNS()
        // seems to not work with XML namespace prefixes.
        let inputXml = document.getElementsByTagName("sos:SOS");

        if (inputXml !== null)
        {
            if (inputXml.length > 0)
            {
                inputXml = inputXml[0].children;

                if (inputXml.length > 0)
                {
                    for (let i = 0; i < inputXml.length; i++)
                    {
                        if (inputXml[i].tagName != "sos:Issue")
                        {
                            console.log("Embedded SOS data: Tag '" + inputXml[i].tagName + "' ignored.");
                            continue;
                        }

                        if (inputXml[i].hasAttribute("id") != true)
                        {
                            throw "Embedded SOS data: Tag '" + inputXml[i].tagName + "' is missing its 'id' attribute.";
                        }

                        let id = inputXml[i].getAttribute("id");

                        if (id.length <= 0)
                        {
                            throw "Embedded SOS data: Empty 'id' attribute of '" + inputXml[i].tagName + "'.";
                        }

                        if (id in _issueMapping)
                        {
                            throw "'id' attribute value '" + id + "' of a '" + inputXml[i].tagName + "' found more than once.";
                        }

                        // Note: In contrast to querySelectorAll(), getElementsByTagName() returns a *live*
                        // HTMLCollection, so changes to the source affect _issueMapping.
                        _issueMapping[id] = inputXml[i];
                        let issueTitle = null;

                        if (inputXml[i].children.length > 0)
                        {
                            for (let j = 0; j < inputXml[i].children.length; j++)
                            {
                                if (inputXml[i].children[j].tagName == "sos:Title")
                                {
                                    if (issueTitle != null)
                                    {
                                        throw "Embedded SOS data: 'Title' tag more than once in 'Issue' tag.";
                                    }

                                    issueTitle = inputXml[i].children[j].textContent;
                                    continue;
                                }

                                if (inputXml[i].children[j].tagName != "sos:Cause" &&
                                    inputXml[i].children[j].tagName != "sos:Effect")
                                {
                                    continue;
                                }

                                if (inputXml[i].children[j].hasAttribute("id") != true)
                                {
                                    throw "Embedded SOS data: Tag '" + inputXml[i].children[j].tagName + "' is missing its 'id' attribute.";
                                }

                                let subId = inputXml[i].children[j].getAttribute("id");

                                if (subId.length <= 0)
                                {
                                    throw "Embedded SOS data: Empty 'id' attribute of '" + inputXml[i].children[j].tagName + "'.";
                                }

                                if (subId in _issueMapping)
                                {
                                    throw "'id' attribute value '" + subId + "' of a '" + inputXml[i].children[j].tagName + "' found more than once.";
                                }

                                // This deliberately maps the child ID of a Cause/Effect to the parent Issue element.

                                // Note: In contrast to querySelectorAll(), getElementsByTagName() returns a *live*
                                // HTMLCollection, so changes to the source affect _issueMapping.
                                _issueMapping[subId] = inputXml[i];
                            }
                        }

                        if (issueTitle == null)
                        {
                            issueTitle = "Title?";
                        }

                        _issueList[id] = issueTitle;
                    }

                    _loaded = true;
                }
                else
                {
                    console.log("Embedded SOS data: Is empty.");
                }
            }
        }
    }

    let _loaded = false;
    let _issueMapping = new Array();
    let _issueList = new Array();
}

function Poi(id, title, description)
{
    let that = this;

    that.getId = function() {
        return _id;
    }

    that.getTitle = function() {
        return _title;
    }

    that.getText = function() {
        return _description;
    }

    let _id = id;
    let _title = title;
    let _description = description;
}

function Issue(id, title, description, causes, effects)
{
    // TODO: Properly check if causes and effects are Arrays, and throw exception if not.

    Poi.call(this, id, title, description);

    let that = this;

    that.getCauses = function() {
        return _causes;
    }

    that.getEffects = function() {
        return _effects;
    }

    let _causes = causes;
    let _effects = effects;
}

function Cause(id, title, description)
{
    Poi.call(this, id, title, description);
}

function Effect(id, title, description)
{
    Poi.call(this, id, title, description);
}
