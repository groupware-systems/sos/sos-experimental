/* Copyright (C) 2019 Stephan Kreutzer
 *
 * This file is part of SOS Grid Prototype 3.
 *
 * SOS Grid Prototype 3 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * SOS Grid Prototype 3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with SOS Grid Prototype 3. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function RenderIssue(issue)
{
    let target = document.getElementById('current');

    if (target == null)
    {
        throw "Renderer: Can't find target with ID 'current'.";
    }

    target.setAttribute("class", "current");

    let container = document.createElement("div");

    let header = document.createElement("h3");
    header.appendChild(document.createTextNode("Issue"));
    container.appendChild(header);

    let title = document.createTextNode(issue.getTitle());

    let span = document.createElement("span");
    span.setAttribute("onclick", "gridNavigator.ShowDetails(\"" + issue.getId() + "\", \"current\");");
    span.appendChild(title);

    container.appendChild(span);

    container.appendChild(document.createElement("br"));
    container.appendChild(document.createElement("br"));

    let navigationLink = document.createElement("a");
    navigationLink.setAttribute("href", "javascript:void(0);");
    navigationLink.setAttribute("onclick", "gridNavigator.NavigateList();");

    let arrow = document.createTextNode("List of Issues");
    navigationLink.appendChild(arrow);
    container.appendChild(navigationLink);

    target.appendChild(container);

    target = document.getElementById('causes');

    if (target == null)
    {
        throw "Renderer: Can't find target with ID 'causes'.";
    }

    container = document.createElement("div");

    header = document.createElement("h3");
    header.appendChild(document.createTextNode("Causes"));
    container.appendChild(header);

    for (let i = 0; i < issue.getCauses().length; i++)
    {
        let title = document.createTextNode(issue.getCauses()[i].getTitle());

        let span = document.createElement("span");
        span.setAttribute("onclick", "gridNavigator.ShowDetails(\"" + issue.getCauses()[i].getId() + "\", \"cause\");");
        span.appendChild(title);

        container.appendChild(span);
        container.appendChild(document.createElement("hr"));
    }

    target.appendChild(container);

    target = document.getElementById('effects');

    container = document.createElement("div");

    header = document.createElement("h3");
    header.appendChild(document.createTextNode("Effects"));
    container.appendChild(header);

    for (let i = 0; i < issue.getEffects().length; i++)
    {
        let title = document.createTextNode(issue.getEffects()[i].getTitle());

        let span = document.createElement("span");
        span.setAttribute("onclick", "gridNavigator.ShowDetails(\"" + issue.getEffects()[i].getId() + "\", \"effect\");");
        span.appendChild(title);

        container.appendChild(span);
        container.appendChild(document.createElement("hr"));
    }

    target.appendChild(container);

    return true;
}

function Reset()
{
    let targets = new Array("causes", "current", "effects")

    for (let i = 0; i < targets.length; i++)
    {
        let target = document.getElementById(targets[i]);

        if (target == null)
        {
            throw "Renderer: Can't find target with ID '" + targets[i] + "'.";
        }

        while (target.hasChildNodes() == true)
        {
            target.removeChild(target.lastChild);
        }
    }

    let target = document.getElementById("current");

    if (target === null)
    {
        throw "Renderer: Can't find target with ID 'current'.";
    }

    target.setAttribute("class", "current hidden");

    target = document.getElementById("list");

    if (target === null)
    {
        throw "Renderer: Can't find target with ID 'list'.";
    }

    target.setAttribute("class", "current hidden");
}

function RenderIssueList(issueList)
{
    let target = document.getElementById("list");

    if (target === null)
    {
        throw "Renderer: Can't find target with ID 'list'.";
    }

    let container = document.createElement("div");

    let header = document.createElement("h3");
    header.appendChild(document.createTextNode("Issues"));
    container.appendChild(header);

    for (let id in issueList)
    {
        if (issueList.hasOwnProperty(id) === true)
        {
            let link = document.createElement("a");
            link.setAttribute("href", "javascript:void(0);");
            link.setAttribute("onclick", "gridNavigator.NavigateIssue(\"" + id + "\");");
            link.appendChild(document.createTextNode(issueList[id]));

            container.appendChild(link);
            container.appendChild(document.createElement("br"));
        }
    }

    target.appendChild(container);
}

function RenderShowIssueList()
{
    let target = document.getElementById("list");

    if (target === null)
    {
        throw "Renderer: Can't find target with ID 'list'.";
    }

    target.setAttribute("class", "current");
}

function RenderDetails(poi)
{
    let target = document.getElementById("details");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID 'details'.";
    }

    let container = document.createElement("div");

    let header = document.createElement("h3");
    header.appendChild(document.createTextNode("Details"));
    container.appendChild(header);

    let description = poi.getText();

    if (description != null)
    {
        container.appendChild(document.createTextNode(description));
    }

    target.appendChild(container);

    return 0;
}

function ResetDetails()
{
    let target = document.getElementById("details");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID 'details'.";
    }

    while (target.hasChildNodes() == true)
    {
        target.removeChild(target.lastChild);
    }
}
