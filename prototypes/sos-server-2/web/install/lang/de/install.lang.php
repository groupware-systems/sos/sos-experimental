<?php
/* Copyright (C) 2013-2022 Stephan Kreutzer
 *
 * This file is part of SOS Server Prototype 2.
 *
 * SOS Server Prototype 2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * SOS Server Prototype 2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with SOS Server Prototype 2. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/install/lang/de/install.lang.php
 * @author Stephan Kreutzer
 * @since 2013-09-14
 */



define("LANG_PAGETITLE", "Installation");

// -- Step 0 -------------------------------------------------------------------

define("LANG_STEP0_HEADER", "Installation");
define("LANG_STEP0_INTROTEXT", "Installation des Server-Prototyps 2 für <a href=\"http://untiednations.com/community/plan-sos\"><span xml:lang=\"en\">Shared/Semantic/Structured Open Space</span></a>.");
define("LANG_STEP0_PROCEEDTEXT", "Weiter");

// -- Step 1 -------------------------------------------------------------------

define("LANG_STEP1_HEADER", "Lizenzvertrag");
define("LANG_STEP1_PROCEEDTEXT", "Zustimmen");

// -- Step 2 -------------------------------------------------------------------

define("LANG_STEP2_HEADER", "Datenbankeinstellungen");
define("LANG_STEP2_REQUIREMENTS", "Diese Software benötigt einen laufenden MySQL-Datenbankserver. Füllen Sie bitte die Datenbank-Verbindungseinstellungen aus. Selbige werden in die Datei <tt>\$/libraries/database_connect.inc.php</tt> des Installationsverzeichnisses der Software geschrieben.");
define("LANG_STEP2_HOSTDESCRIPTION", "Adresse des Datenbankservers");
define("LANG_STEP2_USERNAMEDESCRIPTION", "Datenbank-Benutzername");
define("LANG_STEP2_PASSWORDDESCRIPTION", "Passwort dieses Datenbank-Benutzers");
define("LANG_STEP2_DATABASENAMEDESCRIPTION", "Name der Datenbank, welche die Tabellen enthalten wird");
define("LANG_STEP2_TABLEPREFIXDESCRIPTION", "Präfix für Datenbanktabellen (kann leer gelassen werden, wenn keine Namenskollisionen zu erwarten sind – Präfix endet üblicherweise mit einem Unterstrich '_')");
define("LANG_STEP2_SAVETEXT", "Einstellungen speichern");
define("LANG_STEP2_EDITTEXT", "Einstellungen editieren");
define("LANG_STEP2_PROCEEDTEXT", "Einstellungen bestätigen");
define("LANG_STEP2_DBCONNECTSUCCEEDED", "Verbindung zur Datenbank konnte erfolgreich hergestellt werden!");
// Corresponding with LANG_STEP3_DBCONNECTFAILED.
define("LANG_STEP2_DBCONNECTFAILED", "Verbindung zur Datenbank konnte nicht hergestellt werden. Fehlerbeschreibung: ");
// Corresponding with LANG_STEP3_DBCONNECTFAILEDNOERRORINFO.
define("LANG_STEP2_DBCONNECTFAILEDNOERRORINFO", "Keine Fehlerdetails!");
define("LANG_STEP2_DATABASECONNECTFILECREATEFAILED", "Der Versuch, <tt>\$/libraries/database_connect.inc.php</tt> anzulegen, ist fehlgeschlagen!");
define("LANG_STEP2_DATABASECONNECTFILEISWRITABLE", "<tt>\$/libraries/database_connect.inc.php</tt> kann geschrieben werden!");
define("LANG_STEP2_DATABASECONNECTFILEISNTWRITABLE", "<tt>\$/libraries/database_connect.inc.php</tt> kann nicht geschrieben werden! Entweder bestehen keine Schreibrechte auf das Verzeichnis <tt>\$/libraries/</tt>, sodass die Datei nicht angelegt werden kann, oder die Datei <tt>\$/libraries/database_connect.inc.php</tt> existiert bereits und die Berechtigung zum Schreiben der Datei wurde nicht eingeräumt. Womöglich müssen Sie die Berechtigungen für das Verzeichnis <tt>\$/libraries/</tt> und/oder die potentiell existierende Datei <tt>\$/libraries/database_connect.inc.php</tt> per FTP-Programm (CHMOD-Befehl) oder per Remote-Verbindung auf dem Server manuell einrichten. Vergessen Sie nicht, die Berechtigungen wieder zurückzusetzen, nachdem die Installation abgeschlossen wurde.");
define("LANG_STEP2_DATABASECONNECTFILEWRITABLEOPENFAILED", "<tt>\$/libraries/database_connect.inc.php</tt> schien schreibbar zu sein, sie konnte allerdings nicht geöffnet werden!");
define("LANG_STEP2_DATABASECONNECTFILEWRITEFAILED", "<tt>\$/libraries/database_connect.inc.php</tt> schien schreibbar zu sein, konnte erfolgreich geöffnet werden, jedoch ist das tatsächliche Schreiben fehlgeschlagen!");
define("LANG_STEP2_DATABASECONNECTFILEWRITESUCCEEDED", "<tt>\$/libraries/database_connect.inc.php</tt> konnte erfolgreich geschrieben werden!");
define("LANG_STEP2_DATABASECONNECTFILEISNTREADABLE", "<tt>\$/libraries/database_connect.inc.php</tt> kann nicht gelesen werden! Entweder bestehen keine Leserechte auf das Verzeichnis <tt>\$/libraries/</tt>, sodass auf die Datei nicht zugegriffen werden kann, oder der Datei <tt>\$/libraries/database_connect.inc.php</tt> fehlt die Leseberechtigung. Womöglich müssen Sie die Berechtigungen für das Verzeichnis <tt>\$/libraries/</tt> und/oder die Datei <tt>\$/libraries/database_connect.inc.php</tt> per FTP-Programm (CHMOD-Befehl) oder sonstigem Datenübertragungsprogramm auf dem Server manuell einrichten. Vergessen Sie nicht, die Berechtigungen wieder zurückzusetzen, nachdem die Installation abgeschlossen wurde.");
define("LANG_STEP2_DATABASECONNECTFILEISREADABLE", "Geschriebene <tt>\$/libraries/database_connect.inc.php</tt> kann gelesen werden!");
define("LANG_STEP2_DATABASECONNECTFILEDOESNTEXIST", "<tt>\$/libraries/database_connect.inc.php</tt> existiert nicht!");

// -- Step 3 -------------------------------------------------------------------

define("LANG_STEP3_HEADER", "Einrichtung");
define("LANG_STEP3_INITIALIZETEXT", "Einrichten");
define("LANG_STEP3_INITIALIZATIONDESCRIPTION", "Die Software muss nun initial eingerichtet werden. Dabei werden unter anderem die Tabellen in der Datenbank erzeugt.");
define("LANG_STEP3_CHECKBOXDESCRIPTIONDROPEXISTINGTABLES", "Bereits existierende Tabellen löschen (Achtung: kann nicht rückgängig gemacht werden!)");
define("LANG_STEP3_CHECKBOXDESCRIPTIONKEEPEXISTINGTABLES", "Tabellen nur neu anlegen, wenn sie noch nicht vorhanden sind (behält bestehende Tabellen unverändert bei)");
// Corresponding with LANG_STEP2_DBCONNECTFAILED.
define("LANG_STEP3_DBCONNECTFAILED", "Verbindung zur Datenbank konnte nicht hergestellt werden. Fehlerbeschreibung: ");
// Corresponding with LANG_STEP2_DBCONNECTFAILEDNOERRORINFO.
define("LANG_STEP3_DBCONNECTFAILEDNOERRORINFO", "Keine Fehlerdetails!");
define("LANG_STEP3_DBOPERATIONFAILED", "Datenbankoperation fehlgeschlagen. Fehlerbeschreibung: ");
define("LANG_STEP3_DBOPERATIONFAILEDNOERRORINFO", "Keine Fehlerdetails!");
define("LANG_STEP3_DBOPERATIONSUCCEEDED", "Einrichtung erfolgreich vorgenommen!");
define("LANG_STEP3_COMPLETETEXT", "Einrichtung abschließen");

// -- Step 4 -------------------------------------------------------------------

define("LANG_STEP4_HEADER", "Konfiguration");

define("LANG_STEP4_USERINITIALIZATIONDESCRIPTION", "Bitte legen Sie den ersten Benutzer an. Dieser Benutzer ist gleichzeitig der Administrator.");
define("LANG_STEP4_USERNAMEDESCRIPTION", "Benutzername");
define("LANG_STEP4_PASSWORDDESCRIPTION", "Passwort");
define("LANG_STEP4_EMAILDESCRIPTION", "E-Mail-Adresse");
define("LANG_STEP4_DOMAINROOTDESCRIPTION", "Bitte hinterlegen sie, unter welcher Domain und welchem Server-Pfad die Notiz-Software gerade installiert wird. Dieser Pfad wird verwendet, um absolute Links auf das Notiz-System Ihres Servers zusammensetzen zu können.");
define("LANG_STEP4_BUTTONSAVECAPTION", "Konfiguration speichern");
define("LANG_STEP4_DBOPERATIONSUCCEEDED", "Benutzer erfolgreich angelegt.");
define("LANG_STEP4_PROCEEDTEXT", "Konfiguration abschließen");
define("LANG_STEP4_DBOPERATIONFAILED", "Datenbankoperation fehlgeschlagen.");

// -- Step 5 -------------------------------------------------------------------

define("LANG_STEP5_HEADER", "Fertig!");
define("LANG_STEP5_COMPLETETEXT", "Installation erfolgreich abgeschlossen! Die Installationsroutine wird nun versuchen, sich selbst zu löschen. Wenn dies nicht gelingen sollte (Sie sich also nicht auf der Hauptseite anmelden können und wieder zur Installation umgeleitet werden), müssen Sie das Verzeichnis <tt>\$/install/</tt> manuell löschen, mindestens jedoch die Datei <tt>\$/install/install.php</tt>. Anschließend sollte das Anmeldeformular zugänglich sein.");
define("LANG_STEP5_EXITTEXT", "Beenden");


?>
