<?php
/* Copyright (C) 2019  Stephan Kreutzer
 *
 * This file is part of SOS Server Prototype 2.
 *
 * SOS Server Prototype 2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * SOS Server Prototype 2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with SOS Server Prototype 2. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/admin_import.php
 * @brief Imports a file in SOS format, wiping what's
 *     previously in the database.
 * @author Stephan Kreutzer
 * @since 2019-10-19
 */



require_once("./libraries/https.inc.php");
require_once("./libraries/session.inc.php");
require_once("./libraries/user_defines.inc.php");

if ((int)$_SESSION['user_role'] !== USER_ROLE_ADMIN)
{
    header("HTTP/1.1 403 Forbidden");
    exit(-1);
}

require_once("./libraries/database.inc.php");
require_once("./libraries/poi_defines.inc.php");


class SOSParser
{
    public function __construct($tokens)
    {
        $this->tokens = $tokens;
        $this->max = count($tokens);
    }

    public function parse()
    {
        while ($this->cursor < $this->max)
        {
            if (key($this->tokens[$this->cursor]) === "POI")
            {
                $this->HandlePoi();
                continue;
            }
            else
            {
                throw new Exception("SOSParser: Element '".(key($this->tokens[$this->cursor]))."' not supported.");
            }

            $this->cursor += 1;
        }
    }

    protected function HandlePoi()
    {
        $this->poiId = $this->tokens[$this->cursor]["POI"];

        $this->cursor += 1;

        while ($this->cursor < $this->max)
        {
            $name = key($this->tokens[$this->cursor]);

            if ($name === "/POI")
            {
                $this->cursor += 1;
                return;
            }
            else if ($name === "Title")
            {
                $this->HandleTitle();
                continue;
            }
            else if ($name === "Parent")
            {
                $this->HandleParent();
                continue;
            }
            else
            {
                $this->ConsumeElement();
                continue;
            }

            $this->cursor += 1;
        }
    }

    protected function HandleTitle()
    {
        if ($this->poiTitle !== null)
        {
            throw new Exception("SOSParser: 'POI' with more than one 'Title' child.");
        }

        $this->poiTitle = $this->tokens[$this->cursor]["Title"];

        $this->cursor += 1;

        if (key($this->tokens[$this->cursor]) !== "/Title")
        {
            throw new Exception("SOSParser: After handling 'Title' element, '/Title' is expected, but instead, '".(key($this->tokens[$this->cursor]))."' was found.");
        }

        $this->cursor += 1;
    }

    protected function HandleParent()
    {
        if (is_array($this->poiParents) !== true)
        {
            $this->poiParents = array();
        }

        $this->poiParents[] = $this->tokens[$this->cursor]["Parent"];

        $this->cursor += 1;

        if (key($this->tokens[$this->cursor]) !== "/Parent")
        {
            throw new Exception("SOSParser: After handling 'Parent' element, '/Parent' is expected, but instead, '".(key($this->tokens[$this->cursor]))."' was found.");
        }

        $this->cursor += 1;
    }

    protected function ConsumeElement()
    {
        $endName = "/".key($this->tokens[$this->cursor]);

        $this->cursor += 1;

        while ($this->cursor < $this->max)
        {
            if (key($this->tokens[$this->cursor]) === $endName)
            {
                $this->cursor += 1;
                return;
            }
            else
            {
                $this->ConsumeElement();
                continue;
            }

            $this->cursor += 1;
        }
    }

    public function GetPoiId()
    {
        return $this->poiId;
    }

    public function GetPoiTitle()
    {
        return $this->poiTitle;
    }

    public function GetPoiParents()
    {
        return $this->poiParents;
    }

    protected $tokens = null;
    protected $cursor = 0;
    protected $max = 0;

    protected $poiId = null;
    protected $poiTitle = null;
    protected $poiParents = null;
}

$xmlEventStack = array();
$relations = array();
$idMapping = array();

function StartElementHandler($parser, $name, $attribs)
{
    global $xmlEventStack;

    if ($name === "POI")
    {
        if (!empty($xmlEventStack))
        {
            throw new Exception("SAX: While handling start element '".$name."', the element stack wasn't cleared before as a result of encountering the end element for an earlier start element.");
        }

        if (array_key_exists("ID", $attribs) !== true)
        {
            throw new Exception("SAX: While handling start element '".$name."', attribute 'ID' was missing.");
        }

        $xmlEventStack[] = array("POI" => $attribs['ID']);
    }
    else if ($name === "Title")
    {
        if (!empty($xmlEventStack))
        {
            $xmlEventStack[] = array("Title" => "");
        }
    }
    else if ($name === "Parent")
    {
        if (!empty($xmlEventStack))
        {
            if (array_key_exists("Parent-ID", $attribs) !== true)
            {
                throw new Exception("SAX: While handling start element '".$name."', attribute 'Parent-ID' was missing.");
            }

            $xmlEventStack[] = array("Parent" => $attribs['Parent-ID']);
        }
    }
    else
    {
        if (!empty($xmlEventStack))
        {
            $xmlEventStack[] = array($name => null);
        }
    }
}

function EndElementHandler($parser, $name)
{
    global $xmlEventStack;
    global $relations;
    global $idMapping;

    if ($name === "POI")
    {
        if (empty($xmlEventStack))
        {
            throw new Exception("SAX: While handling end element '".$name."', the element stack is empty as there were no other child elements found inside.");
        }

        $xmlEventStack[] = array("/".$name => null);

        // Up to here, the functions acted as a tokenizer,
        // and now we're going to parse via an implicit
        // call tree.

        $parser = new SOSParser($xmlEventStack);
        $parser->parse();

        $poiIdSource = $parser->GetPoiId();
        $poiTitle = $parser->GetPoiTitle();
        $poiParents = $parser->GetPoiParents();

        if ($poiIdSource === null)
        {
            // Should already be made sure by tokenization
            // that there is always an ID attribute.
            throw new Exception("SAX: Internal error.");
        }

        if (array_key_exists($poiIdSource, $relations) === true)
        {
            throw new Exception("POI ID '".$poiIdSource."' multiple times, isn't unique.");
        }

        if ($poiTitle === null)
        {
            throw new Exception("SAX: 'POI' end element without 'Title' element inside.");
        }

        if (is_array($poiParents) !== null)
        {
            $relations[$poiIdSource] = $poiParents;
        }

        if (Database::Get()->IsConnected() !== true)
        {
            throw new Exception("SAX: No database connection.");
        }

        $idPoiImported = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."pois` (`id`)\n".
                                                 "VALUES (?)\n",
                                                 array(NULL),
                                                 array(Database::TYPE_NULL));

        if ($idPoiImported <= 0)
        {
            throw new Exception("SAX: Failed to insert POI into database ('pois').");
        }

        $idMapping[$poiIdSource] = $idPoiImported;

        $idPoiTitleRevision = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."poi_revisions` (`id`,\n".
                                                      "    `text`,\n".
                                                      "    `revision_datetime`,\n".
                                                      "    `id_pois`)\n".
                                                      "VALUES (?, ?, UTC_TIMESTAMP(), ?)\n",
                                                      array(NULL, $poiTitle, $idPoiImported),
                                                      array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_INT));

        if ($idPoiTitleRevision <= 0)
        {
            throw new Exception("SAX: Failed to insert POI into database ('poi_revisions').");
        }

        $xmlEventStack = array();
    }
    else
    {
        if (!empty($xmlEventStack))
        {
            $xmlEventStack[] = array("/".$name => null);
        }
    }
}

function CharacterDataHandler($parser, $data)
{
    global $xmlEventStack;

    $index = count($xmlEventStack)-1;

    if ($index < 0)
    {
        return;
    }

    if (key($xmlEventStack[$index]) === "Title")
    {
        $xmlEventStack[$index]["Title"] .= $data;
    }
}


require_once("./libraries/languagelib.inc.php");
require_once(getLanguageFile("admin_import"));

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n".
     "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>".LANG_PAGETITLE."</title>\n".
     "    <link rel=\"stylesheet\" type=\"text/css\" href=\"./css/mainstyle.css\"/>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div class=\"mainbox\">\n".
     "      <div class=\"mainbox_header\">\n".
     "        <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
     "      </div>\n".
     "      <div class=\"mainbox_body\">\n";

if (isset($_POST['upload']) !== true)
{
    echo "        <p>\n".
         "          ".LANG_IMPORT_DESCRIPTION."\n".
         "        </p>\n".
         "        <form enctype=\"multipart/form-data\" action=\"admin_import.php\" method=\"post\">\n".
         "          <fieldset>\n".
         "            <input type=\"file\" name=\"file\"/><br/>\n".
         "            <input type=\"submit\" name=\"upload\" value=\"".LANG_IMPORT_SUBMIT."\"/><br/>\n".
         "          </fieldset>\n".
         "        </form>\n".
         "        <a href=\"index.php\">".LANG_LINKCAPTION_CANCEL."</a>\n";
}
else
{
    $success = true;

    if (isset($_FILES['file']) !== true)
    {
        echo "        <p>\n".
             "          <span class=\"error\">".LANG_UPLOAD_GENERAL_ERROR."</span>\n".
             "        </p>\n".
             "        <a href=\"admin_import.php\">".LANG_LINKCAPTION_BACK."</a>\n";

        $success = false;
    }

    if ($success === true)
    {
        if ($_FILES['file']['error'] != 0)
        {
            echo "        <p>\n".
                 "          <span class=\"error\">".LANG_UPLOAD_SPECIFIC_ERROR_PRE.htmlspecialchars($_FILES['file']['error'], ENT_XHTML, "UTF-8").LANG_UPLOAD_SPECIFIC_ERROR_POST."</span>\n".
                 "        </p>\n".
                 "        <a href=\"admin_import.php\">".LANG_LINKCAPTION_BACK."</a>\n";

            $success = false;
        }
    }

    if ($success === true)
    {
        /** @todo Save/export/backup what's currently in the database. */
    }

    /*
    if (@move_uploaded_file($_FILES['file']['tmp_name'], $targetPath) !== true)
    {

    }
    */

    // For debugging, internal errors.
    $errorMessage = "";

    $fileHandle = null;

    if ($success === true)
    {
        $fileHandle = fopen($_FILES['file']['tmp_name'], "rb");

        if ($fileHandle == false)
        {
            $errorMessage .= "Error opening uploaded file for reading.";
            $success = false;
        }
    }

    /** @todo This could also use my StAX parser implementation, which I would need to port to PHP from C++. */
    $sax = null;

    if ($success === true)
    {
        $sax = xml_parser_create();

        if ($sax === false)
        {
            $errorMessage .= "Error creating the SAX parser object.";
            $success = false;
        }
    }

    if ($success === true)
    {
        if (xml_parser_set_option($sax, XML_OPTION_CASE_FOLDING, false) !== true)
        {
            xml_parser_free($sax);
            $errorMessage .= "Error setting SAX parser option.";
            $success = false;
        }
    }

    if ($success === true)
    {
        if (xml_set_element_handler($sax, "StartElementHandler", "EndElementHandler") !== true)
        {
            xml_parser_free($sax);
            $errorMessage .= "Error registering event handler function.";
            $success = false;
        }
    }

    if ($success === true)
    {
        if (xml_set_character_data_handler($sax, "CharacterDataHandler") !== true)
        {
            xml_parser_free($sax);
            $errorMessage .= "Error registering event handler function.";
            $success = false;
        }
    }

    if ($success === true)
    {
        if (Database::Get()->IsConnected() !== true)
        {
            xml_parser_free($sax);
            $errorMessage .= "No database connection.";
            $success = false;
        }
    }

    if ($success === true)
    {
        if (Database::Get()->BeginTransaction() !== true)
        {
            xml_parser_free($sax);
            $errorMessage .= "Error starting database transaction.";
            $success = false;
        }
    }

    if ($success === true)
    {
        if (Database::Get()->ExecuteUnsecure("DELETE FROM `".Database::Get()->GetPrefix()."relations` WHERE 1") !== true)
        {
            $errorMessage .= "Error deleting entries from database in 'relations'.";
            $success = false;
            Database::Get()->RollbackTransaction();
        }
    }

    if ($success === true)
    {
        if (Database::Get()->ExecuteUnsecure("DELETE FROM `".Database::Get()->GetPrefix()."poi_revisions` WHERE 1") !== true)
        {
            $errorMessage .= "Error deleting entries from database in 'poi_revisions'.";
            $success = false;
            Database::Get()->RollbackTransaction();
        }
    }

    if ($success === true)
    {
        if (Database::Get()->ExecuteUnsecure("DELETE FROM `".Database::Get()->GetPrefix()."pois` WHERE 1") !== true)
        {
            $errorMessage .= "Error deleting entries from database in 'pois'.";
            $success = false;
            Database::Get()->RollbackTransaction();
        }
    }

    if ($success === true)
    {
        while (feof($fileHandle) === false)
        {
            $input = fread($fileHandle, 4096);

            if ($input !== false)
            {
                try
                {
                    if (xml_parse($sax, $input, false) !== 1)
                    {
                        $errorMessage .= "Error during SAX parsing.";
                        $success = false;
                        Database::Get()->RollbackTransaction();
                        break;
                    }
                }
                catch (Exception $ex)
                {
                    $errorMessage .= "Error during SAX parsing: ".$ex->getMessage();
                    $success = false;
                    Database::Get()->RollbackTransaction();
                    break;
                }
            }
            else
            {
                $errorMessage .= "Error reading uploaded input file.";
                $success = false;
                Database::Get()->RollbackTransaction();
                break;
            }
        }

        if ($success === true)
        {
            xml_parse($sax, "", true);
        }

        xml_parser_free($sax);
        fclose($fileHandle);
    }

    if ($success === true &&
        count($relations) > 0)
    {
        foreach ($relations as $id => $relation)
        {
            if (is_array($relation) === true)
            {
                if (count($relation) > 0)
                {
                    if (array_key_exists($id, $idMapping) !== true)
                    {
                        $errorMessage .= "Failed to resolve source ID to target ID for child.";
                        $success = false;
                        Database::Get()->RollbackTransaction();
                        break;
                    }

                    $idChild = $idMapping[$id];

                    foreach ($relation as $r)
                    {
                        if (array_key_exists($r, $idMapping) !== true)
                        {
                            $errorMessage .= "Failed to resolve source ID to target ID for parent.";
                            $success = false;
                            Database::Get()->RollbackTransaction();
                            break;
                        }

                        $idParent = $idMapping[$r];

                        $idRelationImported = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."relations` (`id`,\n".
                                                                      "    `id_poi_parent`,\n".
                                                                      "    `id_poi_child`)\n".
                                                                      "VALUES (?, ?, ?)\n",
                                                                      array(NULL, $idParent, $idChild),
                                                                      array(Database::TYPE_NULL, Database::TYPE_INT, Database::TYPE_INT));

                        if ($idRelationImported <= 0)
                        {
                            $errorMessage .= "Failed to insert Parent into database ('relations').";
                            $success = false;
                            Database::Get()->RollbackTransaction();
                            break;
                        }
                    }
                }
            }

            if ($success !== true)
            {
                break;
            }
        }
    }

    if ($success === true)
    {
        if (Database::Get()->IsConnected() === true)
        {
            if (Database::Get()->CommitTransaction() !== true)
            {
                $errorMessage .= "Error committing the database transaction.";
                $success = false;
            }
        }
    }

    if ($success === true)
    {
        echo "        <p>\n".
             "          <span class=\"success\">".LANG_IMPORT_SUCCESS."</span>\n".
             "        </p>\n".
             "        <div>\n".
             "          <a href=\"index.php\">".LANG_LINKCAPTION_DONE."</a>\n".
             "        </div>\n";
    }
    else
    {
        echo "        <p>\n".
             "          <span class=\"error\">".LANG_IMPORT_FAILURE."</span>\n".
             "        </p>\n".
             //"        <p><span class=\"error\">".$errorMessage."</span></p>\n".
             "        <div>\n".
             "          <a href=\"admin_import.php\">".LANG_LINKCAPTION_BACK."</a>\n".
             "          <a href=\"index.php\">".LANG_LINKCAPTION_DONE."</a>\n".
             "        </div>\n";
    }
}

echo "      </div>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n";



?>
