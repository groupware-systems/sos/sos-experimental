<?php
/* Copyright (C) 2019  Stephan Kreutzer
 *
 * This file is part of SOS Server Prototype 2.
 *
 * SOS Server Prototype 2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * SOS Server Prototype 2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with SOS Server Prototype 2. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/grid.php
 * @author Stephan Kreutzer
 * @since 2019-10-12
 */



require_once("./libraries/https.inc.php");
require_once("./libraries/poi_management.inc.php");

require_once("./libraries/languagelib.inc.php");
require_once(getLanguageFile("grid"));

header("Content-Type: application/xhtml+xml");

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"en\" lang=\"en\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:sos=\"http://www.untiednations.com/SOS\" xmlns=\"http://www.w3.org/1999/xhtml\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <!--\n".
     "    Copyright (C) 2018-2019 Stephan Kreutzer\n".
     "\n".
     "    This file is part of SOS Server Prototype 2.\n".
     "\n".
     "    SOS Server Prototype 2 is free software: you can redistribute it and/or modify\n".
     "    it under the terms of the GNU Affero General Public License version 3 or any later version,\n".
     "    as published by the Free Software Foundation.\n".
     "\n".
     "    SOS Server Prototype 2 is distributed in the hope that it will be useful,\n".
     "    but WITHOUT ANY WARRANTY; without even the implied warranty of\n".
     "    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the\n".
     "    GNU Affero General Public License 3 for more details.\n".
     "\n".
     "    You should have received a copy of the GNU Affero General Public License 3\n".
     "    along with SOS Server Prototype 2. If not, see <http://www.gnu.org/licenses/>.\n".
     "\n".
     "    The data in the <div id=\"sos-input\"/> is not part of this program,\n".
     "    it's user data that is only processed. A different license may apply.\n".
     "    -->\n".
     "    <title>".LANG_PAGETITLE."</title>\n".
     "    <meta content=\"initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no\" name=\"viewport\"/>\n".
     "    <link rel=\"stylesheet\" type=\"text/css\" href=\"./css/grid_styles.css\"/>\n".
     "    <script type=\"text/javascript\" src=\"./js/sos-grid-engine.js\">//</script>\n".
     "    <script type=\"text/javascript\" src=\"./js/sos-grid-renderer.js\">//</script>\n".
     "    <script type=\"text/javascript\" src=\"./js/sos-grid-navigation.js\">//</script>\n".
     "    <script type=\"text/javascript\">\n".
     "\"use strict\";\n".
     "\n".
     "// As browsers/W3C/WHATWG turned incredibly evil, they might ignore the self-declarative\n".
     "// XML namespace of this document and the given content type in the header, and instead\n".
     "// assume/render \"text/html\", which then fails with JavaScript characters that are\n".
     "// XML/XHTML special characters which need to be escaped, if the file is saved under\n".
     "// a name that happens to end with \".html\" (!!!). Just have the file name end with\n".
     "// \".xhtml\" and it might magically start to work.\n".
     "\n".
     "function loadGrid()\n".
     "{\n".
     "    if (window.location.hash.length &gt; 1)\n".
     "    {\n".
     "        NavigatePoi(window.location.hash.substr(1));\n".
     "    }\n".
     "    else\n".
     "    {\n".
     "        NavigatePoi(\"\");\n".
     "    }\n".
     "}\n".
     "\n".
     "window.onload = function () {\n".
     "    let loadLink = document.getElementById('loadLink');\n".
     "    loadLink.parentNode.removeChild(loadLink);\n".
     "\n".
     "    loadGrid();\n".
     "\n".
     "    if (\"onhashchange\" in window)\n".
     "    {\n".
     "        window.onhashchange = loadGrid;\n".
     "    }\n".
     "};\n".
     "    </script>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div id=\"grid\">\n".
     "      <div id=\"causes\"/>\n".
     "      <div id=\"current\"/>\n".
     "      <div id=\"effects\"/>\n".
     "      <div id=\"details\"/>\n".
     "      <div id=\"loadLink\">\n".
     "        <a href=\"#\" onclick=\"loadGrid();\">Load</a>\n".
     "      </div>\n".
     "    </div>\n".
     "    <div id=\"sos-input\" style=\"display:none;\">\n".
     "      <!-- The data contained in this element and sub-elements is not part of this program, it's user data and might be under a different license than this program. This program also doesn't depend on it or link it as a library, it's only processed. -->\n".
     "      <sos:SOS Language=\"en\" xsi:schemaLocation=\"http://www.untiednations.com/SOS http://www.untiednations.com/XML/SOSMeeting2.xsd\">\n";

$parents = array();
$relations = GetRelations();

if (is_array($relations) === true)
{
    foreach ($relations as $relation)
    {
        if (array_key_exists($relation['id_poi_child'], $parents) !== true)
        {
            $parents[$relation['id_poi_child']] = array();
        }

        $parents[$relation['id_poi_child']][] = $relation['id_poi_parent'];
    }
}

$pois = GetPois();

if (is_array($pois) === true)
{
    $entries = array();

    {
        $lastPoiId = -1;

        foreach ($pois as $poi)
        {
            if ($lastPoiId == (int)$poi['pois_id'])
            {
                continue;
            }
            else
            {
                $lastPoiId = (int)$poi['pois_id'];
            }

            $entries[] = $poi;
        }
    }

    foreach ($entries as $entry)
    {
        echo "        <sos:POI ID=\"".$entry['pois_id']."\">\n".
             "          <sos:Title>".htmlspecialchars($entry['poi_revisions_text'], ENT_XHTML, "UTF-8")."</sos:Title>\n";

        if (array_key_exists($entry['pois_id'], $parents) === true)
        {
            foreach ($parents[$entry['pois_id']] as $parent)
            {
                echo "          <sos:Parent Parent-ID=\"".$parent."\"/>\n";
            }
        }

        echo "        </sos:POI>\n";
    }
}

echo "      </sos:SOS>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n";

?>
