<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2019 Stephan Kreutzer

This file is part of SOS Map Prototype 2.

SOS Map Prototype 2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

SOS Map Prototype 2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with SOS Map Prototype 2. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:sos="http://www.untiednations.com/SOS">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no"/>

  <xsl:template match="/">
    <graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
      <key id="sos-poi-title" for="node" attr.name="label" attr.type="string"/>
      <key id="sos-poi-text" for="node" attr.name="description" attr.type="string"/>
      <graph id="SOS" edgedefault="directed">
        <xsl:apply-templates select="/sos:SOS/sos:POI"/>
        <xsl:apply-templates select="/sos:SOS/sos:POI/sos:Parent"/>
      </graph>
    </graphml>
  </xsl:template>

  <xsl:template match="/sos:SOS/sos:POI">
    <node id="{@ID}">
      <!--
        GraphML doesn't support semantics for the data of a node, just this generic mechanism. Writing
        our own schema as suggested in http://graphml.graphdrawing.org/primer/graphml-primer.html#EXT
        isn't cool and the official standard lacks support for a complex type with lax validation.
      -->
      <data key="sos-poi-title"><xsl:value-of select="./sos:Title//text()"/></data>
      <data key="sos-poi-text"><xsl:value-of select="./sos:Text//text()"/></data>
    </node>
  </xsl:template>

  <xsl:template match="/sos:SOS/sos:POI/sos:Parent">
    <edge id="edge-{position()}" source="{@Parent-ID}" target="{../@ID}"/>
  </xsl:template>

  <xsl:template match="text()"/>

</xsl:stylesheet>
