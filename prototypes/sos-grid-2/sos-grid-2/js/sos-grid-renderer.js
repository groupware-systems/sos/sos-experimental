/* Copyright (C) 2019 Stephan Kreutzer
 *
 * This file is part of SOS Grid Prototype 2.
 *
 * SOS Grid Prototype 2 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * SOS Grid Prototype 2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with SOS Grid Prototype 2. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function RenderIssue(issue)
{
    let target = document.getElementById('current');

    if (target == null)
    {
        throw "Renderer: Can't find target with ID 'current'.";
    }

    let container = document.createElement("div");

    let header = document.createElement("h3");
    header.appendChild(document.createTextNode("Issue"));
    container.appendChild(header);

    if (issue.getPreviousIssueId() != null)
    {
        let navigationLink = document.createElement("a");
        navigationLink.setAttribute("href", "javascript:void(0);");
        // TODO: Prevent injection (any maybe at other places, too).
        navigationLink.setAttribute("onclick", "gridNavigator.NavigateIssue(\"" + issue.getPreviousIssueId() + "\");");

        let arrow = document.createTextNode("prev");
        navigationLink.appendChild(arrow);
        container.appendChild(navigationLink);
    }
    else
    {
        let span = document.createElement("span");
        span.setAttribute("style", "color: gray;");

        let arrow = document.createTextNode("prev");
        span.appendChild(arrow);
        container.appendChild(span);
    }

    container.appendChild(document.createTextNode(" "));

    if (issue.getNextIssueId() != null)
    {
        let navigationLink = document.createElement("a");
        navigationLink.setAttribute("href", "javascript:void(0);");
        // TODO: Prevent injection (any maybe at other places, too).
        navigationLink.setAttribute("onclick", "gridNavigator.NavigateIssue(\"" + issue.getNextIssueId() + "\");");

        let arrow = document.createTextNode("next");
        navigationLink.appendChild(arrow);
        container.appendChild(navigationLink);
    }
    else
    {
        let span = document.createElement("span");
        span.setAttribute("style", "color: gray;");

        let arrow = document.createTextNode("next");
        span.appendChild(arrow);
        container.appendChild(span);
    }

    container.appendChild(document.createElement("br"));
    container.appendChild(document.createElement("br"));

    let title = document.createTextNode(issue.getTitle());

    let span = document.createElement("span");
    span.setAttribute("onclick", "gridNavigator.ShowDetails(\"" + issue.getId() + "\", \"current\");");
    span.appendChild(title);

    container.appendChild(span);
    target.appendChild(container);

    target = document.getElementById('causes');

    if (target == null)
    {
        throw "Renderer: Can't find target with ID 'causes'.";
    }

    container = document.createElement("div");

    header = document.createElement("h3");
    header.appendChild(document.createTextNode("Causes"));
    container.appendChild(header);

    for (let i = 0; i < issue.getCauses().length; i++)
    {
        let title = document.createTextNode(issue.getCauses()[i].getTitle());

        let span = document.createElement("span");
        span.setAttribute("onclick", "gridNavigator.ShowDetails(\"" + issue.getCauses()[i].getId() + "\", \"cause\");");
        span.appendChild(title);

        container.appendChild(span);
        container.appendChild(document.createElement("hr"));
    }

    target.appendChild(container);

    target = document.getElementById('effects');

    container = document.createElement("div");

    header = document.createElement("h3");
    header.appendChild(document.createTextNode("Effects"));
    container.appendChild(header);

    for (let i = 0; i < issue.getEffects().length; i++)
    {
        let title = document.createTextNode(issue.getEffects()[i].getTitle());

        let span = document.createElement("span");
        span.setAttribute("onclick", "gridNavigator.ShowDetails(\"" + issue.getEffects()[i].getId() + "\", \"effect\");");
        span.appendChild(title);

        container.appendChild(span);
        container.appendChild(document.createElement("hr"));
    }

    target.appendChild(container);

    return true;
}

function Reset()
{
    let targets = new Array("causes", "current", "effects")

    for (let i = 0; i < targets.length; i++)
    {
        let target = document.getElementById(targets[i]);

        if (target == null)
        {
            throw "Renderer: Can't find target with ID '" + targets[i] + "'.";
        }

        while (target.hasChildNodes() == true)
        {
            target.removeChild(target.lastChild);
        }
    }
}

function RenderDetails(poi)
{
    let target = document.getElementById("details");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID 'details'.";
    }

    let container = document.createElement("div");

    let header = document.createElement("h3");
    header.appendChild(document.createTextNode("Details"));
    container.appendChild(header);

    let description = poi.getText();
    
    if (description != null)
    {
        container.appendChild(document.createTextNode(description));
    }

    target.appendChild(container);

    return 0;
}

function ResetDetails()
{
    let target = document.getElementById("details");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID 'details'.";
    }

    while (target.hasChildNodes() == true)
    {
        target.removeChild(target.lastChild);
    }
}
