<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2019 Stephan Kreutzer

This file is licensed under the GNU Affero General Public License 3
(https://www.gnu.org/licenses/agpl-3.0.html) + any later version and/or
under the Creative Commons Attribution-ShareAlike 4.0 International
(https://creativecommons.org/licenses/by-sa/4.0/legalcode).
-->
<SOS xmlns="http://www.untiednations.com/SOS" Language="en" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.untiednations.com/SOS http://www.untiednations.com/XML/SOSMeeting1.xsd">
  <Issue id="1">
    <Title>The browser is sandboxed</Title>
    <Text>Our prototypes are implemented in the browser. The issue is the “Same Origin Policy” and the problem that Cross-Origin Resource Sharing requires the cooperation of remote, external entities in advance.</Text>
    <Cause id="2">
      <Title>Remote code execution</Title>
      <Text>The browser executes unchecked code retrieved from remote, external entities. Such code can be malicious. The browser needs to sandbox the execution in order to protect the computer from compromization. Reasons for it are that no software installation is required in advance (dynamic loading and execution on demand), and that the operating systems running the majority of browsers lack the concept of software distribution repositories where code is submitted and checked, as most of the remote code is proprietary, as well as the operating system that supports this model of denying digital freedom to the user, so the code must be obtained from the original author and can’t be fixed (if broken or malicious), to establish and maintain the dependency.</Text>
    </Cause>
    <Cause id="3">
      <Title>Leakage of confidential data</Title>
      <Text>If the browser gets and executes unchecked code from various remote, external entities, it could send the confidential data hold by the other code back from where itself came from and leak it.</Text>
    </Cause>
    <Cause id="4">
      <Title>Business model of online service dependency</Title>
      <Text>Browser vendors tend to be engaged in some form of SaaS online service lock-in dependency business model, so it would harm their business interest if the sandbox wouldn’t enforce the use of these services and instead would allow to make use of local alternatives.</Text>
    </Cause>
    <Effect id="5">
      <Title>Limitations for offline use</Title>
      <Text>Data can’t be loaded from or stored to the local hard drive. It’s impossible to interoperate with existing other software on the system. It’s impossible to retrieve data from or submit data to a remote server.</Text>
    </Effect>
    <Effect id="6">
      <Title>Quasi-reqirement of online use</Title>
      <Text>No data exchange can happen conveniently if it isn’t done with the original remote, external server that delivered the web application files. The operator of the server that delivered the web application files will be faced with responsibilities like data protection, preventing data loss, security, quality of service (uptime and providing the service, so if the service is discontinued, people will loose their work), demands of law enforcement, etc. If the user doesn’t have connectivity, some functions won’t work, for example: uploading a file in order to share/publish and then retrieve it again from the original remote server, because local files can’t be added to the local web software.</Text>
    </Effect>
  </Issue>
  <Issue id="7">
    <Title>No neutral repository</Title>
    <Text>The repository for the prototypes and examples is published under the “groupware-systems” group: https://gitlab.com/groupware-systems/sos/sos-experimental</Text>
    <Cause id="8">
      <Title>Bootstrapping</Title>
      <Text>No prior organizational structures, ad-hoc setup.</Text>
    </Cause>
    <Effect id="9">
      <Title>Bad for linking</Title>
      <Text>Links to the repository will lead to skreutzer’s account. A redirect likely can’t be set up later, so moving the repository later likely will break existing links.</Text>
    </Effect>
    <Effect id="10">
      <Title>Bad context</Title>
      <Text>It’s only a single personal repository on skreutzer’s account, where the context and environment holds many other repositories as well that aren’t related to SOS, so somebody who explores the repository wouldn’t find other SOS material, but unrelated projects, which is bad for the discoverability of the SOS projects. Other SOS repositories aren’t collected in one place, but spread and mixed with other stuff, which is confusing. There is no one-stop-shop authoritative place where all the materials can be found.</Text>
    </Effect>
    <Effect id="32">
      <Title>Bad for community management</Title>
      <Text>It’s difficult and risky for other people to join and participate, if their contributions are bound to a personal account that’s not related to SOS. Connections might get lost for SOS if a transition needs to happen later.</Text>
    </Effect>
    <Effect id="33">
      <Title>Bus factor</Title>
      <Text>With git, forking is easy and cheap, so it’s not an effect of too much importance yet that the experimental repository for examples and prototypes is operated by a single person and in case of mishap, malicious actions or loss of availability/interest might become inacessible.</Text>
    </Effect>
    <Effect id="11">
      <Title>No connections from SOS</Title>
      <Text>There’s no connection from the wcharlton user account nor from the official website http://www.untiednations.com/community/plan-sos/ to the repository, which is bad for discoverability and relies solely on wcharlton’s responsibility to indicate the existence of it, in terms of bus factor.</Text>
    </Effect>
  </Issue>
  <Issue id="12">
    <Title>Unpublished material</Title>
    <Text>Some of William Charlton’s material isn’t published like the grid interface mockup drawings, HTML grid interface mockup, ASPX server prototype, the “SOS - Meeting for Untied Nations/The Ecology of Systems Thinking” and “SOS - Meeting for Cape Town Hackathon” files with their corresponding XSLTs.</Text>
    <Cause id="13">
      <Title>Bootstrapping</Title>
      <Text>No prior organizational structures, ad-hoc setup.</Text>
    </Cause>
    <Cause id="14">
      <Title>No repository</Title>
      <Text>In lack of a shared repository or a personal repository, there is no good place to publish them.</Text>
    </Cause>
    <Cause id="15">
      <Title>Copyright</Title>
      <Text>For the publication of the material to make sense and be useful, it should be licensed properly. This requires William Charlton to consider the licenses and deciding on one or several of them.</Text>
    </Cause>
    <Effect id="16">
      <Title>Incompleteness, not open</Title>
      <Text>People who look into the project via the site or the sos-experimental repository won’t be able to get an overview of what has been produced so far. Potential collaborators or independent contributors won’t have access to all the files and might start to waste time by producing similar material which was already produced, but isn’t available.</Text>
    </Effect>
    <Effect id="17">
      <Title>No versioning</Title>
      <Text>It’s difficult to keep track of the different versions of a file and it can’t easily be established from a given file if it is current or an historic version.</Text>
    </Effect>
    <Effect id="18">
      <Title>Backup is difficult</Title>
      <Text>It takes a lot of effort to spread the individual files in order to have copies of the whole collection everywhere as a backup mechanism if the original repository or (e-mail) accounts become unavailable.</Text>
    </Effect>
  </Issue>
  <Issue id="19">
    <Title>SOS formats differ</Title>
    <Text>Stephan Kreutzer’s example files differ from William Charlton’s example files. Stephan Kreutzer’s prototypes expect a different format than William Charlton’s prototypes expect. Stephan Kreutzer’s example files don’t validate according to the official XSD.</Text>
    <Cause id="20">
      <Title>Bootstrapping</Title>
      <Text>No prior discussion, fast prototyping. Saving the time that would be required for co-designing. Experimentation to potentially adjust/improve the final format.</Text>
    </Cause>
    <Effect id="21">
      <Title>Not interoperable</Title>
      <Text>Because of differing formats, the prototypes are incompatible and example files are specific to only one of the two prototype groups. The examples can’t be demonstrated to work with all of the prototypes.</Text>
    </Effect>
  </Issue>
  <Issue id="22">
    <Title>What about Meetings?</Title>
    <Text>It’s unclear what role Meetings should play. They’re in the format, but examples were produced without a meeting (or how is “Meeting” defined?) and the prototypes can be used without as well.</Text>
    <Cause id="23">
      <Title>SOS is also a human system tool</Title>
      <Text>SOS planning is a method/process/procedure, not just a software tool. It’s intended to be used in specific contexts and setups. The prototypes don’t reflect that yet.</Text>
    </Cause>
    <Cause id="24">
      <Title>Is pre-existing</Title>
      <Text>The Meeting is in William Charlton’s example files and prototypes that pre-existed the creation of this Issue.</Text>
    </Cause>
    <Effect id="25">
      <Title>Meetings not reflected in all examples and prototypes</Title>
      <Text>Stephan Kreutzer’s examples and prototypes don’t reflect the concept of the Meeting, which leads to incompatibility with William Charlton’s examples and prototypes that do implement the concept of the Meeting.</Text>
    </Effect>
  </Issue>
  <Issue id="26">
    <Title>Progress isn’t visible</Title>
    <Text>Visitors and potential collaborators/contributors can’t find out about the progress of the project.</Text>
    <Cause id="27">
      <Title>Bootstrapping</Title>
      <Text>No prior organizational structures, ad-hoc setup.</Text>
    </Cause>
    <Effect id="28">
      <Title>Not very engaging</Title>
      <Text>Visitors, potential collaborators and potential contributors might not get attracted because of thinking that the project is inactive.</Text>
    </Effect>
  </Issue>
  <Issue id="29">
    <Title>No production system</Title>
    <Text>The examples and prototypes are somewhat fixed/frozen, they serve as mere examples that shouldn’t be polluted with expansion as a result of real-world usage in order to remain useful and concise, with focus on specific features and characteristics. For the real bootstrapping of this project effort, there’s no production system/environment that also allows organizational enhancements like notifications or versioning to track the history.</Text>
    <Cause id="30">
      <Title>Bootstrapping</Title>
      <Text>No prior organizational structures, ad-hoc setup.</Text>
    </Cause>
    <Effect id="31">
      <Title>Progress is limited</Title>
      <Text>Bootstrapping can’t improve and advance to a higher stage as the existing results aren’t supposed to and don’t contribute to the actual project effort itself.</Text>
    </Effect>
  </Issue>
</SOS>
